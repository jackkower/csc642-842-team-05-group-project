//the App.js page is where everything goes through. It acts at the entry point.
import React from "react";
import "./App.css";
//import react-bootstrap to use in App page
// import {
//   Button,
//   ButtonGroup,
//   Dropdown,
//   DropdownButton,
//   Form,
//   FormControl,
//   Nav,
//   Navbar,
//   SplitButton,
// } from "react-bootstrap";
// import bootstrap so react-bootstrap can use the css dependency
import "bootstrap/dist/css/bootstrap.min.css";

function SearchResults() {
  return (
    <h1>Results</h1>
  );
}
export default SearchResults;