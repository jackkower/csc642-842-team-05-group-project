//the App.js page is where everything goes through. It acts at the entry point.
import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./Home.js";
import SearchResults from "./SearchResults";
import {
  Button,
  ButtonGroup,
  Dropdown,
  DropdownButton,
  Form,
  FormControl,
  Nav,
  Navbar,
  SplitButton,
} from "react-bootstrap";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Testi from "./assets/testi.JPG";

function App() {
  return (
    (<SearchResults />),
    (
      <main>
        <Switch>
          <Route path="/" component={Home} />
          <Route path="/searchResults" component={SearchResults} />
        </Switch>
      </main>
    ),
    (
      <div className="App">
        <Navbar bg="light" variant="light">
          <Navbar.Brand href="#home">CityScape</Navbar.Brand>
          <Nav className="mr-auto">
            <Form inline>
              <div className="mb-2">
                {[DropdownButton].map((DropdownType, idx) => (
                  <DropdownType
                    as={ButtonGroup}
                    key={idx}
                    id={`dropdown-button-drop-${idx}`}
                    size="md"
                    title="Cities"
                  >
                    <Dropdown.Item eventKey="1">Atlanta</Dropdown.Item>
                    <Dropdown.Item eventKey="2">Chicago</Dropdown.Item>
                    <Dropdown.Item eventKey="3">New York City</Dropdown.Item>
                    <Dropdown.Item eventKey="5">Philadelphia</Dropdown.Item>
                    <Dropdown.Item eventKey="6">San Diego</Dropdown.Item>
                    <Dropdown.Item eventKey="7">San Francisco</Dropdown.Item>
                    <Dropdown.Item eventKey="8">Seattle</Dropdown.Item>
                    <Dropdown.Item eventKey="9">Washiongton D.C.</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item eventKey="10">London</Dropdown.Item>
                    <Dropdown.Item eventKey="12">Tokyo</Dropdown.Item>
                  </DropdownType>
                ))}
              </div>
              <div className="mb-2">
                {[DropdownButton].map((DropdownType, idx) => (
                  <DropdownType
                    as={ButtonGroup}
                    key={idx}
                    id={`dropdown-button-drop-${idx}`}
                    size="md"
                    title="Docent type"
                  >
                    <Dropdown.Item eventKey="1">Athlete</Dropdown.Item>
                    <Dropdown.Item eventKey="7">Food Lover</Dropdown.Item>
                    <Dropdown.Item eventKey="2">Hipster</Dropdown.Item>
                    <Dropdown.Item eventKey="3">Professional</Dropdown.Item>
                    <Dropdown.Item eventKey="5">Relaxer</Dropdown.Item>
                    <Dropdown.Item eventKey="6">Thrill seeker</Dropdown.Item>
                  </DropdownType>
                ))}
              </div>
              <FormControl
                type="text"
                placeholder="Search"
                className="mr-sm-2"
              />
              <Button variant="outline-primary">Submit</Button>
            </Form>
            <Nav.Link href="#itinerary">itinerary</Nav.Link>
            <Nav.Link href="#getDocent">Get a Docent</Nav.Link>
            <Nav.Link href="#setDocent">Become a Docent</Nav.Link>
            <Nav.Link href="#login">Login</Nav.Link>
            <Nav.Link href="#signin">Sign Up</Nav.Link>
            <Nav.Link href="#">About</Nav.Link>
          </Nav>
        </Navbar>
        <div className="home">
          <h1>CityScape</h1>
          <h2>Escape to The City</h2>
          <h3>
            Travel With a Docent! We Provide Docents who can help you help
            around the city.
          </h3>
          <h3>
            Get a personal docent for a trip or talk to a Docent temporarily.
          </h3>
          <h3>
            Do you know your city well? Why not get paid?<b>Become a Docent!</b>
          </h3>
        </div>
        <div>
          <img src={Testi} alt="testi-image" className="testi" />
        </div>
        <header className="App-header"></header>
      </div>
    )
  );
}

export default App;
